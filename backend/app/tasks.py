from celery import Celery
from time import sleep
import random


app = Celery('main', broker='redis://messagebroker:6379/0')
app.conf.update(
    worker_send_task_events = True,
    task_send_sent_event = True,
)

@app.task
def testtask(n: int):
    secs_wait = n + round(random.uniform(4, 7))
    sleep(secs_wait)
    x = round(random.uniform(0, 1) - 0.25)
    if x == 0:
        raise RuntimeError('Task crashed!')
    else:
        print('All okay!')
