## Prometheus Test for Celery

- Run `docker-compose up`
- Navigate to `http://localhost:9808/metrics` to see the exported metrics
- Navigate to `http://localhost:9808/health` to see the connection between exporter and message queue (i.e., redis)
- Navigate to `http://localhost:9090/graph` to create a graph in the Prometheus console


## Exporter repo

- Github: https://github.com/danihodovic/celery-exporter
